console.log("Hello World!");

function miniActivity(){
    let nickname = prompt("Enter you nickname");
    console.log(nickname);
}
miniActivity();

function printName(name){
    console.log("Hello " + name);
};

printName("Joana");
printName("John");
printName("Jane");

let sampleVar = "Yua";
printName(sampleVar);

function printName(){
    console.log("Hello " + name);
};

printName("Joana");
printName("John");
printName("Jane");

let sampleVar = "Yua";
printName(sampleVar);

function printNumbers(param1, param2){
    console.log("The numbers passed as arguments are: ");
    console.log(param1);    
    console.log(param2);    
};

// printNumbers(5,2);  // undefined is only 5 is indicated

// function printFriends(friend1, friend2, friend3){
//     console.log("My friends are " + friend1 + " " + friend2 + " " + friend3);
// }

// printFriends("Martin", "Kevin", "Jerome");

// // // **Improving our codes from
// // function checkDivisibilityBy8(num){
// //     let remainder = num%8;
// //     // console.log("The remainder of " + num + " is " + remainder);
// //     let isDivisibleBy8 = remainder === 0;
// //     // console.log("Is " + num + " divisible by 8?");
// //     console.log(isDivisibleBy8);
// // }
// // checkDivisibilityBy8(64);
// // checkDivisibilityBy8(28);

// // // function checkDivisibilityBy4(num){
// // //     let remainder = num%4;
// // //     console.log("The remainder of " + num + " is " + remainder);
// // //     let isDivisibleBy4 = remainder === 0;
// // //     console.log("Is " + num + " divisible by 4?");
// // //     console.log(isDivisibleBy4);
// // // }
// // // checkDivisibilityBy4(40);
// // // // checkDivisibilityBy8(28);

// function isEven(num){
// 	console.log(num%2) 
//     let numEven = isEven(20);
//     console.log(numEven);
// }

// function isOdd(num1){
//     num2/2 !== 0;
//     let numOdd = isOdd(31);
//     console.log(numOdd);
// }

// isEven(20);
// isOdd(19);

// // // *********** Start of Debugging
// // // function isEven(num){
// // // 	console.log(number % 2 = 0) 
// // // }

// // // function isOdd(num1){
// // // 	num2/2 !== 0;
// // // }

// // // let numEven = isEven(20);
// // let numOdd = isOdd(31);

// // console.log(numEven);
// // console.log(numOdd);
// // ************ End of Debugging

// // ************ Start of Answers 
// // ************ # 1
// // function isEven(num){
// //     console.log(num % 2 === 0);
// // };
// // isEven(20);

// // ************ # 2
// // function isOdd(num1){
// //     console.log(num1 % 2 !== 0);
// // }

// // isEven(20);
// // isEven(21);
// // isOdd(31);
// // isOdd(30);
// // ************ End of Answers

// // ***FUNCTIONS AS ARGUMENTS

// function argumentFunction(){
//     console.log("This function is passed into another function.");
// };

// function invokeFunction(){
//     functionParameter();
// };

// invokeFunction(argumentFunction);

// function argumentFunction(){
//     console.log("This function is passed into another function.");
// };

// function invokeFunction(functionParameter){
//     functionParameter();
// };

// invokeFunction(argumentFunction);

// function printFullName(firstName, middleName, lastName){
//     console.log(firstName + " " + middleName + " " + lastName);
// }
// printFullName("Juan", "Dela", "Cruz");

// **Trying to achieve the same result as the function above ie Juan Dela Cruz
// let firstName = "Juan";
// let middleName = "Dela";
// let lastName = "Cruz";
// console.log(firstName + " " + middleName = " " + lastName);

// RETURN STATEMENT
// ###ends function execution
// ###without return, you wont get anything

// ******************
// function returnFullName(firstName, middleName, lastName){
//     return firstName + " " + middleName + " " + lastName;
//     console.log("This message will not be printed");
// }
// returnFullName("Jeffrey", "Smith", "Bezos");
// ****************** Nothing happened. Not printed

// ****************** 
function returnFullName(firstName, middleName, lastName){
    return firstName + " " + middleName + " " + lastName;
    // console.log("This message will not be printed");
}
console.log(returnFullName("Jeffrey", "Smith", "Bezos"));

// ****************** same output as above
// the value returned from a function can also be stored inside a variable
function returnFullName(firstName, middleName, lastName){
    return firstName + " " + middleName + " " + lastName;
    // console.log("This message will not be printed");
}
let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);

// MINIACTIVITY

// function returnAddress(city, country){
//     return city + ", " + country;
// }
// console.log(returnAddress("Manila", "Philippines"));

// *************
// function returnAddress(city, country){
//     let fullAddress = city + ", " + country;
//     return fullAddress;
// }
// let myAddress = returnAddress("Manila", "Philippines")
// console.log(myAddress);
// *************

function printPlayerInfo(username, level, job){
    console.log("Username: " + username);
    console.log("Level: " + level);
    console.log("Job: " + job);
}
let user1 = printPlayerInfo("knight_white", 95, "Paladin");
console.log(user1);


