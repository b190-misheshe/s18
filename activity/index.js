// console.log("Hello World!");
	
	// 1.  Create a function which will be able to add two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of the addition in our console.
	// 	-function should only display result. It should not return anything.

	// 	Create a function which will be able to subtract two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of subtraction in our console.
	// 	-function should only display result. It should not return anything.

	// 	-invoke and pass 2 arguments to the addition function
	// 	-invoke and pass 2 arguments to the subtraction function

        function sumNumbers(sum1, sum2){
            console.log("Displayed sum of " + sum1 + " and " + sum2);
            console.log(sum1 + sum2);
        };
        sumNumbers(5,15);

        function differenceNumbers(difference1, difference2){
            console.log("Displayed difference of " + difference1 + " and " + difference2);
            console.log(difference1 - difference2);
        }
        differenceNumbers(20,5);

	// 2.  Create a function which will be able to multiply two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the multiplication.

	// 	Create a function which will be able to divide two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the division.


        function returnProduct(product1, product2){
            console.log("The product of " + product1 + " and " + product2 + ":");
            return product1 * product2;
        }
        // console.log(returnProduct(50,10));

        function returnQuotient(quotient1, quotient2){
            console.log("The quotient of " + quotient1 + " and " + quotient2 + ":");
            return quotient1 / quotient2
        }
        // console.log(returnQuotient(50,10));


	//  	Create a global variable called outside of the function called product.
	// 		-This product variable should be able to receive and store the result of multiplication function.
	// 	Create a global variable called outside of the function called quotient.
	// 		-This quotient variable should be able to receive and store the result of division function.

	// 	Log the value of product variable in the console.
	// 	Log the value of quotient variable in the console.

        let product = returnProduct(50,10);   //50,10
        console.log(product);

        let quotient = returnQuotient(50,10);  //50,10
        console.log(quotient);

	// 3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
	// 		-a number should be provided as an argument.
	// 		-look up the formula for calculating the area of a circle with a provided/given radius.
	// 		-look up the use of the exponent operator.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the area calculation.

	// 	Create a global variable called outside of the function called circleArea.
	// 		-This variable should be able to receive and store the result of the circle area calculation.

	// Log the value of the circleArea variable in the console.
    
        function finalCircleArea (radius){
            console.log("The result of getting the area of a circle with " + radius + " radius:");  
            const resultCircleArea = Math.PI * radius ** 2;
            return resultCircleArea;
        };
        let circleArea = finalCircleArea (15);
         console.log(circleArea);


	// 4. 	Create a function which will be able to get total average of four numbers.
	// 		-4 numbers should be provided as an argument.
	// 		-look up the formula for calculating the average of numbers.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the average calculation.

	//     Create a global variable called outside of the function called averageVar.
	// 		-This variable should be able to receive and store the result of the average calculation
	// 		-Log the value of the averageVar variable in the console.


        function totalAverage(ave1, ave2, ave3, ave4){
            console.log("The average of " + ave1 + ", " + ave2 + ", " + ave3 + " and " + ave4 + ":");
            const average = (ave1 + ave2 + ave3 + ave4) / 4;
            return average;
        }
        let averageVar = totalAverage (20,40,60,80);
        console.log(averageVar);

	// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
	// 		-this function should take 2 numbers as an argument, your score and the total score.
	// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
	// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
	// 		-return the value of the variable isPassed.
	// 		-This function should return a boolean.

		// Create a global variable called outside of the function called isPassingScore.
		// 	-This variable should be able to receive and store the boolean result of the checker function.
		// 	-Log the value of the isPassingScore variable in the console.

        function percentageScore (yourScore, totalScore){
            console.log("Is " + yourScore + "/" + totalScore + " a passing score?");
            const testScore = (yourScore/totalScore) * 100;
            isScorePassed = testScore > 75;
            let isPassed = isScorePassed;
            return isPassed;
        };
        let isPassingScore = percentageScore(38,50);
        console.log(isPassingScore);